def es_entero_no_negativo(valor):
    try:
        valor = int(valor)
        return valor >= 0
    except ValueError:
        return False

def suma_impares_entre_numeros(numero1, numero2):
    suma = 0
    for num in range(numero1, numero2 + 1):
        if num % 2 != 0:
            suma += num
    return suma

while True:
    numero1 = input("Dame un número entero no negativo: ")
    if not es_entero_no_negativo(numero1):
        print("¡Inténtalo de nuevo!")
    else:
        numero1 = int(numero1)
        break

while True:
    numero2 = input("Dame otro: ")
    if not es_entero_no_negativo(numero2):
        print("¡Inténtalo de nuevo!")
    else:
        numero2 = int(numero2)
        break

resultado = suma_impares_entre_numeros(numero1, numero2)
print(f"La suma de los números impares entre {numero1} y {numero2} es: {resultado}")